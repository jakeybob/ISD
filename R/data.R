#' Health Board Identifier Dataset
#'
#' Dataset for testing code related to health board identifiers.
#'
#' @docType data
#'
#' @usage data(HBdata)
#'
#' @format An object of class \code{"tibble"} and \code{"dataframe"}
#' (see \code{\link{data.frame}}). Consists of four columns:
#'
#' \describe{
#'  \item{name}{A descriptive character value, e.g. "NHS Forth Valley"}
#'  \item{cipher}{A single character identifier, e.g. "V"}
#'  \item{code}{An alphanumeric value, e.g. "S08000019"}
#'  \item{altcode}{An alphanumeric value, e.g. "SVA20"}
#' }
#'
#' @keywords datasets, ISD
#'
#' @source ISD package
#'
#' @examples
#' data(HBdata)
"HBdata"


#' OPCS4 Dummy Dataset
#'
#' Dataset for testing code related to OPCS codes. Note that these are randomly generated dummy codes
#' that have the same alphanumeric pattern of OPCS4 codes, but do not generally correspond to real codes or procedures.
#'
#' @docType data
#'
#' @usage data(OPdata)
#'
#' @format An object of class \code{"tibble"} and \code{"dataframe"}
#' (see \code{\link{data.frame}}). Consists of four columns:
#'
#' \describe{
#'  \item{MAIN_OPERATION}{An 8 character alphanumeric of the form "A123B456".}
#'  \item{OTHER_OPERATION_1}{An 8 character alphanumeric of the form "A123B456".}
#'  \item{OTHER_OPERATION_2}{An 8 character alphanumeric of the form "A123B456".}
#'  \item{OTHER_OPERATION_3}{An 8 character alphanumeric of the form "A123B456".}
#' }
#'
#' @keywords datasets, ISD
#'
#' @source ISD package
#'
#' @examples
#' data(OPdata)
"OPdata"
